//
//  CallDirectoryHandler.swift
//  BlacklistCallExtension
//
//  Created by Anton Sokolov on 01.08.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import Foundation
import CallKit

class CallDirectoryHandler: CXCallDirectoryProvider {

    override func beginRequest(with context: CXCallDirectoryExtensionContext) {
        context.delegate = self

        // Check whether this is an "incremental" data request. If so, only provide the set of phone number blocking
        // and identification entries which have been added or removed since the last time this extension's data was loaded.
        // But the extension must still be prepared to provide the full set of data at any time, so add all blocking
        // and identification phone numbers if the request is not incremental.
        print("context.isIncremental: ", context.isIncremental)
        if context.isIncremental {
            addOrRemoveIncrementalBlockingPhoneNumbers(to: context)

            addOrRemoveIncrementalIdentificationPhoneNumbers(to: context)
        } else {
            addAllBlockingPhoneNumbers(to: context)

            addAllIdentificationPhoneNumbers(to: context)
        }

        context.completeRequest()
    }

    private func addAllBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        // Retrieve all phone numbers to block from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
        //
        // Numbers must be provided in numerically ascending order.
        let allPhoneNumbers: [CXCallDirectoryPhoneNumber] = []
        for phoneNumber in allPhoneNumbers {
            context.addBlockingEntry(withNextSequentialPhoneNumber: phoneNumber)
        }
    }

    private func addOrRemoveIncrementalBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        // Retrieve any changes to the set of phone numbers to block from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = []
        for phoneNumber in phoneNumbersToAdd {
            context.addBlockingEntry(withNextSequentialPhoneNumber: phoneNumber)
        }

        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = []
        for phoneNumber in phoneNumbersToRemove {
            context.removeBlockingEntry(withPhoneNumber: phoneNumber)
        }

        // Record the most-recently loaded set of blocking entries in data store for the next incremental load...
    }

    private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        print("addAllIdentificationPhoneNumbers")
        // Retrieve phone numbers to identify and their identification labels from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
        //
        // Numbers must be provided in numerically ascending order.
//        let allPhoneNumbers: [CXCallDirectoryPhoneNumber] = [ 79219455081 ]
//        let labels = [ "Тест номер" ]
//
//        for (phoneNumber, label) in zip(allPhoneNumbers, labels) {
//            context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
        
        guard let fileUrl = FileManager.default
            .containerURL(forSecurityApplicationGroupIdentifier: "group.deasoft.BlacklistApp")?
            .appendingPathComponent("AllNumbers") else { return }
        
        guard let reader = LineReader(path: fileUrl.path) else { return }
        
        for line in reader {
            autoreleasepool {
                // считываем строку и удаляем перевод строки в конце
                let line = line.trimmingCharacters(in: .whitespacesAndNewlines)
                
                // отделяем номер от имени
                var components = line.components(separatedBy: ",")
                
                // приводим номер к Int64
                guard let phoneNumber = Int64(components[0]) else { return }
                let label = components[1]
                
                //print("phone: ", phoneNumber, "label: ", label)
                context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
            }
        }
//        }
    }

    private func addOrRemoveIncrementalIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        print("addOrRemoveIncrementalIdentificationPhoneNumbers")
        // Retrieve any changes to the set of phone numbers to identify (and their identification labels) from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
//        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = [ 79219455081 ]
//        let labelsToAdd = [ "Тест номер 2" ]
//
//        for (phoneNumber, label) in zip(phoneNumbersToAdd, labelsToAdd) {
//            context.addIdentificationEntry(withNext	PhoneNumber: phoneNumber, label: label)
//        }
//
//        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = [ 1_888_555_5555 ]
//
//        for phoneNumber in phoneNumbersToRemove {
//            context.removeIdentificationEntry(withPhoneNumber: phoneNumber)
//        }
        context.removeAllIdentificationEntries()
        
        guard let fileUrl = FileManager.default
            .containerURL(forSecurityApplicationGroupIdentifier: "group.deasoft.BlacklistApp")?
            .appendingPathComponent("AllNumbers") else { return }
        
        guard let reader = LineReader(path: fileUrl.path) else { return }
        
        for line in reader {
            autoreleasepool {
                // считываем строку и удаляем перевод строки в конце
                let line = line.trimmingCharacters(in: .whitespacesAndNewlines)
                
                // отделяем номер от имени
                var components = line.components(separatedBy: ",")
                
                // приводим номер к Int64
                guard let phoneNumber = Int64(components[0]) else { return }
                let label = components[1]
                
                //print("phone: ", phoneNumber, "label: ", label)
                context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
            }
        }

        // Record the most-recently loaded set of identification entries in data store for the next incremental load...
    }

}

extension CallDirectoryHandler: CXCallDirectoryExtensionContextDelegate {

    func requestFailed(for extensionContext: CXCallDirectoryExtensionContext, withError error: Error) {
        // An error occurred while adding blocking or identification entries, check the NSError for details.
        // For Call Directory error codes, see the CXErrorCodeCallDirectoryManagerError enum in <CallKit/CXError.h>.
        //
        // This may be used to store the error details in a location accessible by the extension's containing app, so that the
        // app may be notified about errors which occured while loading data even if the request to load data was initiated by
        // the user in Settings instead of via the app itself.
    }

}
