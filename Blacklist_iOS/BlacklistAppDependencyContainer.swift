//
//  BlacklistAppDependencyContainer.swift
//  Blacklist
//
//  Created by Anton Sokolov on 29.08.2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import UIKit

public class BlacklistAppDependencyContainer {
    // Main
    // Factories needed to create a MainViewController.
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    public func makeMainViewController() -> UIViewController {
        guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else { return UIViewController() }
        
        if (userDefaults.object(forKey: "LicenseAgreement") as? Date) == nil {
            //show the onboard screen
            return storyboard.instantiateViewController(withIdentifier: "OnboardingVC")
        } else {
            //show the main screen
            return storyboard.instantiateInitialViewController()!
        }
    }
}
