//
//  ShareViewController.swift
//  BlacklistShareExtension
//
//  Created by Anton Sokolov on 04.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices
import Contacts
import Security
import Alamofire
import os.log

class ShareViewController: SLComposeServiceViewController {
    
    //var number = "Not set"
    var contact: CNContact?
    private var phoneDecks = [SLComposeSheetConfigurationItem]()
    private var phones = [Phone]()
    fileprivate var selectedPhone: Phone?
    private var commentTypes = [CommentType]()
    fileprivate var selectedType: CommentType?
    let messageMaxLength = 250
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPhoneNumbers(completionHandler: {responce  in
            self.phones = responce
            print("=== Телефоны получены ===")
            self.reloadConfigurationItems()
        })
        
        if !Net.shared.isConnectedToNetwork() {
            placeholder = NSLocalizedString("No_internet_connection_message", comment: "")
            return
        }
        
        Net.shared.getToken(completionHandler: {
            result in
            switch result {
            case .success:
                print("Токен получен")
                Net.shared.getTypes(completionHandler: {responce, success  in
                    if success {
                        print("=== Типы получены ===")
                        self.commentTypes = responce
                    } else {
                        print("=== Ошибка при получении типов ===")
                    }
                })
                
            case .failure(let error):
                print(error)
                let alertController = UIAlertController(title: "Error", message:
                    error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
        })
        
        placeholder = NSLocalizedString("Write_your_own_review", comment: "")
    }
    
    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        print("Проверяем контент")
        
        let messageLength = contentText.count
        charactersRemaining = messageMaxLength - messageLength as NSNumber
        
        if self.selectedPhone != nil && self.selectedType != nil && !contentText.isEmpty && messageLength <= messageMaxLength {
            return true
        } else {
            return false
        }
    }
    
    override func didSelectPost() {
        
        guard let name = contentText, let phone = selectedPhone?.numberString, let type_id = selectedType?.id else {
            os_log("Пытался сохранить комментарий", log: OSLog.default, type: .debug)
            fatalError("Unable to save comment")
        }
        
        print("######################")
        print("#\(name)")
        print("#\(phone)")
        print("#\(type_id)")
        print("######################")
        
        savePhoneComment(phone: phone, name: name, type_id: type_id)
        
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
        
        //        let url:URL = URL(string: "blacklist://\(String(describing: selectedPhone?.number))")!
        //        let _ = self.openURL(url)
        
    }
    
    override func configurationItems() -> [Any]! {
        
        //        let imageView = UIImageView(image: UIImage(named: "vurb-icon-rounded"))
        //        imageView.contentMode = .scaleAspectFit
        //        navigationItem.titleView = imageView
        //        navigationController?.navigationBar.topItem?.titleView = imageView
        //        navigationController?.navigationBar.tintColor = .white
        //        navigationController?.navigationBar.backgroundColor = UIColor(red:0.39, green:0.46, blue:0.86, alpha:1.00)
        
        phoneDecks = []
        
        guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else {
            return nil
        }
        
        if (userDefaults.object(forKey: "LicenseAgreement") as? Date) == nil {
            
            let controller: UIViewController
            controller = LicenseViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            
            return nil
        }
        
        while phones.isEmpty {
            print("=== Телефоны еще не инициализированы ===")
            Thread.sleep(forTimeInterval: 0.1)
        }
        print("=== Телефоны инициализированы ===")
        
        //
        if let deck = SLComposeSheetConfigurationItem() {
            deck.value = ""
            if phones.count == 1 {
                selectedPhone = phones.first
                deck.title = selectedPhone?.numberFormatted
            } else if selectedPhone != nil {
                deck.title = selectedPhone!.numberFormatted
                deck.tapHandler = {
                    let vc = ShareSelectPhoneViewController()
                    vc.phones = self.phones
                    vc.delegate = self
                    self.pushConfigurationViewController(vc)
                }
            } else {
                deck.title = NSLocalizedString("Select_number", comment: "")
                deck.tapHandler = {
                    let vc = ShareSelectPhoneViewController()
                    vc.phones = self.phones
                    vc.delegate = self
                    self.pushConfigurationViewController(vc)
                }
            }
            phoneDecks = [deck]
        }
        
        if selectedPhone != nil && (selectedPhone?.isCorrectInternationalNumber())!{
            if let deck2 = SLComposeSheetConfigurationItem() {
                deck2.title = (selectedType != nil) ? selectedType?.name : NSLocalizedString("Select_type", comment: "")
                deck2.value = ""
                deck2.tapHandler = {
                    let vc = ShareSelectTypeViewController()
                    vc.commentTypes = self.commentTypes
                    vc.delegate = self
                    self.pushConfigurationViewController(vc)
                }
                phoneDecks.append(deck2)
            }
        }
        
        if selectedPhone != nil && (selectedPhone?.isCorrectInternationalNumber())!{
            if let deck3 = SLComposeSheetConfigurationItem() {
                deck3.title = NSLocalizedString("Comments", comment: "")
                deck3.value = ""
                deck3.tapHandler = {
                    let vc = ShareReadCommentsViewController()
                    vc.selectedPhone = self.selectedPhone
//                    vc.delegate = self
                    self.pushConfigurationViewController(vc)
                }
                phoneDecks.append(deck3)
            }
        }
        
        print("=== Деки отрисованы ===")
        return phoneDecks
    }
    
    //  Function must be named exactly like this so a selector can be found by the compiler!
    //  Anyway - it's another selector in another instance that would be "performed" instead.
    @objc func openURL(_ url: URL) -> Bool {
        var responder: UIResponder? = self
        while responder != nil {
            if let application = responder as? UIApplication {
                return application.perform(#selector(openURL(_:)), with: url) != nil
            }
            responder = responder?.next
        }
        return false
    }
    
    private func getPhoneNumbers(completionHandler: @escaping (_ phones: [Phone]) -> Void) {
        // Получаем номера телефонов выбранного контакта
        for content in extensionContext!.inputItems as! [NSExtensionItem] {
            guard let attachments = content.attachments else {
                return
            }
            
            for attachment in attachments {
                attachment.loadItem(forTypeIdentifier: "public.vcard", options: nil) { item, error in
                    if let data = item as? NSData {
                        if let contact = (try! CNContactVCardSerialization.contacts(with: data as Data) as [CNContact]).first {
                            self.contact = contact
                            print("#\n#\n#")
                            var phonesArray = [Phone]()
                            for phone in contact.phoneNumbers {
                                guard let p = Phone(number: String(describing: (phone.value).stringValue)) else {
                                    Net.shared.saveEvent(name: "ShareViewController.swift: 226", description: "Unable to create phone object in isContentValid", phone: "\(phone.value)")
                                    fatalError("Unable to create phone object in isContentValid")
                                }
                                phonesArray.append(p)
                                //print("Number: \(String(describing: (phone.value).stringValue))")
                            }
                            completionHandler(phonesArray)
                            //                            print("name: \(contact.givenName) - number: \(String(describing: (contact.phoneNumbers.first?.value)!.stringValue))")
                            //                            print("#")
                            //                            self.number = "\(String(describing: (contact.phoneNumbers.first?.value)!.stringValue))"
                        }
                    }
                }
            }
        }
        
    }
    
    
    private func savePhoneComment(phone: String, name: String, type_id: Int) {
//        print("Добавляем комментарий")
//        let commentsUrl = "https://ktozvonit.org/rest/comments/"
//        let sURL = commentsUrl
//        let url:URL = URL(string: sURL)!
//
//        let parameters:[String: Any] = ["phone": phone, "name": name, "type_id": type_id]
//
//        Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: {
//            responce in
//            switch responce.result {
//            case .success:
//                print("Комментарий успешно сохранен")
//
//            case .failure(let error):
//                print(error)
//            }
//       })
        
        
        Net.shared.saveComment(phone: phone, name: name, type_id: type_id, completionHandler: {
            result, responce in
            switch result {
            case .success:
                print("Комментарий успешно сохранен")
            case .failure(let error):
                Net.shared.saveEvent(name: "ShareViewController.swift: 270", description: "\(error)", phone: "")
                let alertController = UIAlertController(title: "Error", message:
                    error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                return
            }})
    }
    
}

extension ShareViewController: ShareSelectPhoneViewControllerDelegate {
    func selected(phone: Phone) {
        selectedPhone = phone
        reloadConfigurationItems()
        popConfigurationViewController()
        validateContent()
    }
}

extension ShareViewController: ShareSelectTypeViewControllerDelegate {
    func selectedType(type: CommentType) {
        selectedType = type
        reloadConfigurationItems()
        popConfigurationViewController()
        validateContent()
    }
}
