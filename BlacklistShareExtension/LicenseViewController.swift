//
//  LicenseViewController.swift
//  BlacklistShareExtension
//
//  Created by Anton Sokolov on 24.09.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import UIKit

class LicenseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        let button = UIButton(type: UIButton.ButtonType.system)
        button.frame = CGRect(x: 0, y: self.view.frame.height - 48, width: 80, height: 40)
        button.center.x = self.view.center.x
        //button.backgroundColor = UIColor.blue
        //button.layer.cornerRadius = 8
        button.setTitle("Ok", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
        
        let label = UILabel()
        label.frame = CGRect(x: 8, y: 8, width: self.view.frame.maxX-16, height: 120)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 3
        label.textAlignment = NSTextAlignment.center
//        label.layer.borderColor = UIColor.red.cgColor
//        label.layer.borderWidth = 1
        label.text = NSLocalizedString("License_Agreement_Extension", comment: "")
        self.view.addSubview(label)
        
    }

    
    @objc func buttonAction(sender: UIButton!) {
        // Закрываем окно
        self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
    }


}
