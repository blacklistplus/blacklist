//
//  ShareSelectPhoneViewController.swift
//  TestProject
//
//  Created by Anton Sokolov on 05.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit

protocol ShareSelectPhoneViewControllerDelegate: class {
    func selected(phone: Phone)
}

class ShareSelectPhoneViewController: UIViewController {
    
    //var commentDecks = [Deck]()
    var phones = [Phone]()
    weak var delegate: ShareSelectPhoneViewControllerDelegate?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.frame)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Identifiers.DeckCell)
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        title = NSLocalizedString("Select_number", comment: "")
        view.addSubview(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

private extension ShareSelectPhoneViewController {
    struct Identifiers {
        static let DeckCell = "deckCell"
    }
}

extension ShareSelectPhoneViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phones.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.DeckCell, for: indexPath)
        cell.textLabel?.text = phones[indexPath.row].numberFormatted
        cell.backgroundColor = .clear
        return cell
    }
}

extension ShareSelectPhoneViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selected(phone: phones[indexPath.row])
    }
}
