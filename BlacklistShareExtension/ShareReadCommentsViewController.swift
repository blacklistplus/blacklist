//
//  ShareReadCommentsViewController.swift
//  Blacklist
//
//  Created by Anton Sokolov on 10.09.17.
//  Copyright © 2017 MVK, OOO. All rights reserved.
//

import UIKit


class ShareReadCommentsViewController: UIViewController {
    
    var comments = [Comment]()
    var selectedPhone: Phone?
    
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), style: .plain)
        
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Identifiers.CommentsDeckCell)
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Comments", comment: "")

        if !Net.shared.isConnectedToNetwork() {
            return
        }
        //Получаем комментарии
        Net.shared.getCommentsByNumber(number: (self.selectedPhone?.number)!, completionHandler: {
            result, comments, provider, region in
            //self.view.frame.height = 568.0
            self.comments = comments
            self.view.addSubview(self.tableView)
            //            self.tableView.frame = myFrame
            
            //            self.tableView.contentOffset = CGPoint(x:0.0, y:44.0)
            //            self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 44.0, 0.0)
            self.tableView.estimatedRowHeight = 44 //tableView.rowHeight
            //            self.tableView.rowHeight = UITableViewAutomaticDimension
            //self.tableView.sizeToFit()
            //self.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !Net.shared.isConnectedToNetwork() {
            let label = UILabel()
            label.frame = CGRect(x: 8, y: 8, width: self.view.frame.maxX-16, height: 120)
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 3
            label.textAlignment = NSTextAlignment.center
            label.text = NSLocalizedString("No_internet_connection_message", comment: "")
            self.view.addSubview(label)
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

private extension ShareReadCommentsViewController {
    struct Identifiers {
        static let CommentsDeckCell = "commentsDeckCell"
    }
}

extension ShareReadCommentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.isEmpty ? 1 : comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.CommentsDeckCell, for: indexPath)
        
        cell.textLabel?.numberOfLines = 0
        
        if comments.isEmpty {
            cell.textLabel?.text = NSLocalizedString("Comments_are_not_found", comment: "")
        } else {
            cell.textLabel?.text = comments[indexPath.row].name
        }
        cell.backgroundColor = .clear
        return cell
    }
}


