//
//  ShareCommentsViewController.swift
//  TestProject
//
//  Created by Anton Sokolov on 05.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit

protocol ShareSelectTypeViewControllerDelegate: class {
    func selectedType(type: CommentType)
}

class ShareSelectTypeViewController: UIViewController {
    
    var commentTypes = [CommentType]()
    weak var delegate: ShareSelectTypeViewControllerDelegate?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.frame)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Identifiers.TypesDeckCell)
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.blue]
        title = NSLocalizedString("Select_type", comment: "")
        view.addSubview(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

private extension ShareSelectTypeViewController {
    struct Identifiers {
        static let TypesDeckCell = "typeDeckCell"
    }
}

extension ShareSelectTypeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.commentTypes.isEmpty) ? 1 : commentTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.TypesDeckCell, for: indexPath)
        
        
        if self.commentTypes.isEmpty {
            cell.textLabel?.text = NSLocalizedString("Connection_error", comment: "")
        } else {
            cell.textLabel?.text = commentTypes[indexPath.row].name
        }
        cell.backgroundColor = .clear
        return cell
    }
}

extension ShareSelectTypeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedType(type: commentTypes[indexPath.row])
    }
}

