//
//  CommentViewController.swift
//  Blacklist Project
//
//  Created by Anton Sokolov on 31.08.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import PhoneNumberKit
import os.log

class CommentViewController: UIViewController, UITextViewDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet weak var selectTypeButton: UIButton!
    

    
    /*
     This value is either passed by `CommentTableViewController` in `prepare(for:sender:)`
     */
    var comment: Comment?
    var commentType: CommentType? {
        didSet {
            guard let commentType = commentType else {
                selectTypeButton.setTitle(NSLocalizedString("Select_type", comment: ""), for: .normal)
                return
            }
            selectTypeButton.setTitle(commentType.name, for: .normal)
        }
    }
    var phoneNumber: PhoneNumber!
    var number: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveComment))
        
        
        //self.setupHideKeyboardOnTap() //dismiss keyboard by tapping outside
        
        nameTextView.delegate = self
        
        updateSaveButtonState()
        nameTextView.placeholder = NSLocalizedString("Feedback_placeholder", comment: "")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let userInfoKey = notification.userInfo!["UIKeyboardAnimationDurationUserInfoKey"] as! Float
        if(userInfoKey == 0.0 ) {return}
        if let keyboardFrame:NSValue = (notification.userInfo! as NSDictionary).value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardHeight
                self.view.layoutIfNeeded()
            })
        }

    }
    
    @objc func keyboardWillHide(notification: NSNotification) {        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        
    }
    
    @objc func appMovedToBackground() {
        view.endEditing(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    // MARK: UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("Текст изменился")
        print(textView.text)
    }
    
    
    // MARK: - Navigation
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMealMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The CommentViewController is not inside a navigation controller.")
        }
    }
    
    // This method lets you configure a view controller before it's presented.
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        guard let identifier = segue.identifier else {
            return
        }
        
        switch identifier {
        case "toTypes":
            // Select comment type, nothing to prepare
            break
        case "unwindToCommentsTable":
            
            // Configure the destination view controller only when the save button is pressed.
            guard let button = sender as? UIBarButtonItem, button === navigationItem.rightBarButtonItem else {
                ErrorHandler.shared.reportError(message: "CommentViewController: 150")
                return
            }
            
            let name = nameTextView.text ?? ""
            guard let type_id = commentType?.id else {
                ErrorHandler.shared.reportError(message: "CommentViewController: 156")
                return
            }
            
            comment = Comment(name: name, phone: number, type_id: type_id)
            
            break
        default:
            return
        }

    }
    
    //MARK: Actions
    
    @IBAction func unwindToCommentView(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? TypesTableViewController, let selectedType = sourceViewController.selectedType {
            self.commentType = selectedType
            updateSaveButtonState()
        }
    }
    
    
    //MARK: Private Methods
    
    private func updateSaveButtonState() {
        // Disable the Save button if the text of the comment is empty.
        // Or type_id not choosen
        //let text = self.nameTextView.text ?? ""
        guard commentType?.id != nil else {
            //saveButton.isEnabled = false
            navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        //print("Длина текста:", text.count)
        navigationItem.rightBarButtonItem?.isEnabled = true //!text.isEmpty
    }
    
    @objc func saveComment() {
        self.performSegue(withIdentifier: "unwindToCommentsTable", sender: navigationItem.rightBarButtonItem)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//extension UIViewController {
//    /// Call this once to dismiss open keyboards by tapping anywhere in the view controller
//    func setupHideKeyboardOnTap() {
//        self.view.addGestureRecognizer(self.endEditingRecognizer())
//        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
//    }
//
//    /// Dismisses the keyboard from self.view
//    private func endEditingRecognizer() -> UIGestureRecognizer {
//        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
//        tap.cancelsTouchesInView = false
//        return tap
//    }
//}
