//
//  SyncProcessor.swift
//  Blacklist
//
//  Created by Anton Sokolov on 06/03/2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import YandexMobileMetrica

class SyncProcessor {
    var netManager = NetManager.shared
    private var isSyncStarted = false
    private var syncProgress = 0
    var backgroundTaskID: Int?
    var isSettingsOn: Bool?
    var startTime: Date = Date()
    var isReachableViaWiFi = false
    
    static let shared = SyncProcessor()
    private init() { }
    
    func getSyncStatus() -> Bool {
        return isSyncStarted
    }
    
    func getSyncProgress() -> Int {
        return syncProgress
    }
    
    var isLicenseAccepted: Bool {
        get {
            guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else {
                return false
            }
            return userDefaults.object(forKey: "LicenseAgreement") as? Date != nil
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                
                ErrorHandler.shared.reportError(message: "SyncProcessor: 74 \(error), \(error.userInfo)")
                return
            }
        })
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return container
    }()
    
    lazy var viewContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    lazy var updateContext: NSManagedObjectContext = {
        let _updateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        _updateContext.parent = self.viewContext
        return _updateContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                Net.shared.saveEvent(name: "Blacklist.swift: 73", description: "Unresolved error \(nserror), \(nserror.userInfo)", phone: "")
            }
        }
    }
    
    func saveUpdateContext() {
        let context = updateContext
        if context.hasChanges {
            do {
                try context.save()
                viewContext.performAndWait {
                    do {
                        try viewContext.save()
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    
    /// Сохраняет массив телефонов в локальную базу
    /// Если номер в базе уже есть, то сравнивает поле editedon и при необходимости обновляет
    ///
    /// - parameter phones:        Массив номеров.
    ///
    /// - returns: Void.
    func processPhones(phones: [Phone]) {
        //Обрабатываем полученные телефоны
        for phone in phones {
            
            // Creates a task with a new background context created on the fly
            //persistentContainer.performBackgroundTask { (context) in
            //Проверяем есть ли такой телефон в локальной базе
            let predicate = NSPredicate(format: "number == %@", phone.number)
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Number")
            request.predicate = predicate
            request.returnsObjectsAsFaults = false
            request.fetchLimit = 1
            
            let newNumber = Number(context: updateContext)
            newNumber.number = Int64(phone.number)!
            newNumber.type = phone.type
            newNumber.type_id = phone.type_id
            newNumber.editedon = Int64(phone.editedon)
            if(phone.label.isEmpty) {
                newNumber.label = phone.type
            } else {
                newNumber.label = phone.label
            }
        }
    }
    
    
    /// Подсчитывае количество записей в локальной БД
    ///
    /// - returns: Void.
    func countDB() {
        persistentContainer.performBackgroundTask { (context) in
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Number")
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try context.fetch(request)
                print("Номеров в базе:", result.count)
                
            } catch {
                Net.shared.saveEvent(name: "Blacklist.swift: 207", description: "processPhones Failed", phone: "")
                print("processPhones Failed")
            }
        }
        
    }
    
    private func SyncTask(offset: Int, updated: Int, handler: @escaping ((Int)->Void)) {
        netManager.getPhones(offset: offset, updated: updated)
            .done { (phones, offset, limit, total, serverTimestamp) in
                // Отправляем статистику в яндекс метрику
                if offset == 0 {
                    let result = "Server Timestamp: \(serverTimestamp) Local Timestamp: \(updated), Total: \(total)"
                    ErrorHandler.shared.reportEvent(message: "Update in progress", parameters: result)
                }
                
                if(phones.count > 0) {
                    // Обрабатываем полученные телефоны
                    // Записываем их в базу
                    self.processPhones(phones: phones)
                    self.saveUpdateContext()
                    UserDefaults.standard.set(offset + phones.count, forKey: "Offset")
                    
                    
                    print("ServerTimestamp: \(serverTimestamp) LocalTimestamp: \(updated) Total: \(total) ToProcess: \(total - offset - phones.count)")
                }
                
                if limit == 0 {
                    // Ошибка связи, повторяем запрос
                    let result = "ServerTimestamp: \(serverTimestamp) LocalTimestamp: \(updated), Total: \(total) Offset: \(offset)"
                    ErrorHandler.shared.reportEvent(message: "Ошибка связи. Отправляем повторный запрос.", parameters: result)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) { // delay before next try
                        self.SyncTask(offset: offset, updated: updated) { total in
                            let result = "ServerTimestamp: \(serverTimestamp) LocalTimestamp: \(updated), Total: \(total) Offset: \(offset)"
                            ErrorHandler.shared.reportEvent(message: "Повторный запрос успешно выполнен", parameters: result)
                            handler(total)
                        }
                    }
                    
                    return
                }
                
                if total > offset + phones.count {
                    // запрашиваем следующую партию номеров
                    self.syncProgress = Int((offset + phones.count) * 100 / total)

                    self.SyncTask(offset: offset + phones.count, updated: updated) { total in
                        print("SyncTask завершена offset", offset)
                        handler(total)
                    }
                } else {
                    // процесс обновления закончился
                    
                    UserDefaults.standard.set(0, forKey: "Offset")
                    UserDefaults.standard.set(serverTimestamp, forKey: "Updated")
                    
                    let updateTime = Date().timeIntervalSince(self.startTime)
                    let result = "Server Timestamp: \(serverTimestamp) Local Timestamp: \(updated), Total: \(total), Duration: \(updateTime)"
                    if total > 0 {
                        ErrorHandler.shared.reportEvent(message: "Update installed", parameters: result)
                    }
                    self.syncProgress = 0
                    
                    // End the task assertion.

                    if let backgroundTaskID = self.backgroundTaskID {                        UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTaskID))
                        self.backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
                    }
                    
                    handler(total)
                }
                
            }
            .catch {
                error in
                handler(0)
        }
        
    }
    
    func Sync(handler: @escaping ((Int)->Void)) {
        
        if(isSyncStarted == true) { // может быть запущен только один sync процесс
            handler(0)
            return
        }
        
        isSyncStarted = true
        startTime = Date()
        ErrorHandler.shared.reportEvent(message: "Update started")
        
        // Perform the task on a background queue.
        backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish Sync Task") {
            // End the task if time expires.
            UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(self.backgroundTaskID!))
            self.backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
            }.rawValue
        
        // Send the data.
        
        let updated = (UserDefaults.standard.value(forKey: "Updated") as? Int) ?? 0
        let offset = (UserDefaults.standard.value(forKey: "Offset") as? Int) ?? 0
        
        print("Пошла синхронизация. offset: \(offset) updated: \(updated)")
        
        background {
            self.SyncTask(offset: offset, updated: updated) { total in
                let updateTime = Date().timeIntervalSince(self.startTime)
                
                self.countDB()
                self.isSyncStarted = false
                
                
                // Обновляем Call Kit
                DispatchQueue.main.async {
                    Blacklist.shared.CheckUserSettings(completionHandler: { result in
                        if(result == true) {
                            // Если доступ к CallKit разрешен обновляем данные
                            Blacklist.shared.updateCallDirectory()
                            
                            let result = "total: \(total), wifi: \(self.isReachableViaWiFi) Duration: \(updateTime)"
                            ErrorHandler.shared.reportEvent(message: "Call Kit Updated", parameters: result)
                        }
                    })
                }
                
                handler(total)
            }
        }

    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
    return UIBackgroundTaskIdentifier(rawValue: input)
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
    return input.rawValue
}
