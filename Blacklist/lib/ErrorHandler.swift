//
//  ErrorHandler.swift
//
//  Created by Anton Sokolov on 22.08.2019.
//  Copyright © 2019 Anton Sokolov. All rights reserved.
//

import Foundation
import YandexMobileMetrica
import YandexMobileMetricaCrashes
import os.log

class ErrorHandler {
    static var shared = ErrorHandler()
    
    let yandexApiKey = "c4f29f8a-0500-4c75-baab-bfc6884e6cb1"
    
    private var debug = false
    let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
    let dateFormatter = DateFormatter()
    
    private init() {
        print("Инициализация AppMetrica SDK")
        // Инициализация AppMetrica SDK.
        #if DEBUG
        self.debug = true
        #else
        // Инициализируем AppMetrika SDK
        let configuration = YMMYandexMetricaConfiguration.init(apiKey: yandexApiKey)
        YMMYandexMetrica.activate(with: configuration!)
        
        #endif
        
    }
    
    func reportError(message: String) {
        
        let date = dateFormatter.string(from: Date())
        
        let result = "\(date)  version: \(appVersion)"
        let params : [String : Any] = [message: result]
        
        if(debug) {
            os_log("[Error] %{PUBLIC}@", log: OSLog.default, type: .error, message)
        } else {
            YMMYandexMetrica.reportEvent("Error", parameters: params, onFailure: nil)
        }
        
    }
    
    func reportEvent(message: String, parameters: String = "") {
        
        let date = dateFormatter.string(from: Date())
        
        let result = "\(date)  version: \(appVersion) " + parameters
        let params : [String : Any] = [message: result]
        
        if(debug) {
            os_log("[Event] %{PUBLIC}@", log: OSLog.default, type: .default, message + ":" + result)
        } else {
            YMMYandexMetrica.reportEvent("Event", parameters: params, onFailure: nil)
        }
    }
    
}
