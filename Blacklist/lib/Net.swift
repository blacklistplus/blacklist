//
//  Net.swift
//  Blacklist
//
//  Created by Anton Sokolov on 31.07.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import Foundation
import Alamofire
import SystemConfiguration

class Net {
    
    let debug = true
    
    let phonesUrl = "https://api.ktozvonit.org/rest/phones/"
    let commentsUrl = "https://api.ktozvonit.org/rest/comments/"
    let keyUrl = "https://api.ktozvonit.org/getkey.html"
    let typesUrl = "https://api.ktozvonit.org/rest/types/"
    let eventsUrl = "https://api.ktozvonit.org/rest/events/"
    private var token: String?
    
    var isToken: Bool {
        return token != nil
    }
    
    static let shared = Net()
    private init() { }
    
    //MARK: - GET Methods
    
    func getCommentsByNumber(number: String, completionHandler: @escaping (Result<Any>, _ comments: [Comment], _ provider: String?, _ region: String?) -> Void) {
        
        if(token == nil) {
            Net.shared.saveEvent(name: "Net: 101", description: "Token is nil", phone: number)
            return
        }
        
        let sURL = commentsUrl + "?search[number]=" + number.numbers + "&token=\(token!)"
        let url:URL = URL(string: sURL)!
        
        Alamofire.request(url).responseJSON(completionHandler: {
            responce in
            switch responce.result {
            case .success(let value):
                guard let data = value as? [String: AnyObject], let rows = data["data"] as? [[String: AnyObject]] else {
                    return
                }
                
                print("rows:", rows)
                
                let comments = rows.map { Comment(with: $0) }.compactMap { $0 }
                print("comments:", comments)
                
                // Пытаемся получить оператора и регион
                // Если их нет подставляем название страны
                guard let provider = data["operator"] as? String, let region = data["region"] as? String else {
                    completionHandler(responce.result, comments, nil, nil)
                    return
                }
                
                completionHandler(responce.result, comments, provider, region)
                //Uncomment for debugginng
                //Thread.sleep(forTimeInterval: 3)
                
            case .failure(let error):
                ErrorHandler.shared.reportError(message: "Net: 129 \(error.localizedDescription)")
                completionHandler(responce.result, [], "", "")
            }
        })
        
    }
    
    func getTypes(completionHandler: @escaping (_ types: [CommentType], _ success: Bool) -> Void) {
        if(token == nil) {
            print("Token is nil")
            Net.shared.saveEvent(name: "Net.swift: 153", description: "Token is nil", phone: "")
            return
        }
        let sURL = typesUrl + "?token=\(token!)"
        let url:URL = URL(string: sURL)!
        
        Alamofire.request(url).responseJSON(completionHandler: {
            responce in
            switch responce.result {
            case .success:
                
                guard let value = responce.result.value as? [String: AnyObject],
                    let rows = value["data"] as? [[String: AnyObject]] else {
                        Net.shared.saveEvent(name: "Net.swift: 154", description: "Malformed data received from fetchAllRooms service", phone: "")
                        Net.shared.saveEvent(name: "Net.swift: 154", description: "response: \(responce.result.value!)", phone: "")
                        print("Net.swift 154: Malformed data received from fetchAllRooms service", responce.result.value!)
                        
                        return
                }
                
                var types = [CommentType]()
                
                for row in rows {
                    if row["id"] != nil && row["name"] != nil {
                        guard let id = row["id"] as? Int, let name = row["name"] as? String else {
                            Net.shared.saveEvent(name: "Net.swift: 178", description: "Error while getting types", phone: "")
                            print("Что то пошло не так: типы комментариев не получены.")
                            
                            return
                        }
                        
                        guard let commentType = CommentType(id: id, name: name) else {
                            Net.shared.saveEvent(name: "Net.swift: 185", description: "Unable to instantiate comment", phone: "")
                            fatalError("Unable to instantiate comment")
                        }
                        
                        types.append(commentType)
                    }
                }
                
                completionHandler(types, true)
                
                
            case .failure(let error):
                Net.shared.saveEvent(name: "Net.swift: 197", description: error as! String, phone: "")
                print(error)
                completionHandler([], false)
            }
        })
        
    }
    
    func getToken(completionHandler: @escaping (Result<String>) -> Void) -> Void {
        let sURL = keyUrl
        let url:URL = URL(string: sURL)!
        
        Alamofire.request(url).responseString(completionHandler: {
            responce in
            switch responce.result {
            case .success:
                guard let key = responce.result.value else {
                    ErrorHandler.shared.reportError(message: "Net.swift: 234")
                    return
                }
                
                self.token = key
                
            case .failure(let error):
                ErrorHandler.shared.reportError(message: "Net.swift: 241 \(error.localizedDescription)")
            }
            completionHandler(responce.result)
        })
    }
    
    //MARK: - POST Methods
    
    func saveComment(phone: String, name: String, type_id: Int, completionHandler: @escaping (Result<Any>, _ comment: Comment) -> Void) {
        if(token == nil) {
            Net.shared.saveEvent(name: "Net.swift: 235", description: "Token is nil", phone: "")
            print("Token is nil")
            return
        }
        let sURL = commentsUrl
        let url:URL = URL(string: sURL)!
        
        let parameters:[String: Any] = ["phone": phone, "name": name, "type_id": type_id, "token": token!]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: {
            responce in
            
            print("responce:", responce)
            switch responce.result {
            case .success:
                guard let value = responce.result.value as? [String: AnyObject],
                    let data = value["data"] as? [String: AnyObject] else {
                        Net.shared.saveEvent(name: "Net.swift: 251", description: "Malformed data received from fetchAllRooms service", phone: "")
                        print("Net.swift 231: Malformed data received from fetchAllRooms service", responce)
                        return
                }
                
                
                
                if data["name"] != nil && data["phone"] != nil {
                    guard let number = data["phone"] as? String, let name = data["name"] as? String, let avatar = data["avatar"] as? String, let phonetype = data["type"] as? String, let date = data["createdon"] as? String else {
                        Net.shared.saveEvent(name: "Net.swift: 260", description: "Error", phone: "")
                        print("что то пошло не так")
                        return
                    }
                    
                    guard let comment = Comment(name: name, phone: number, avatar: avatar, type: phonetype, date: date) else {
                        Net.shared.saveEvent(name: "Net.swift: 266", description: "Unable to instantiate comment", phone: "")
                        fatalError("Unable to instantiate comment")
                    }
                    completionHandler(responce.result, comment)
                }
                
            case .failure(let error):
                print(error)
                guard let comment = Comment(name: name, phone: phone, type_id: type_id) else {
                    Net.shared.saveEvent(name: "Net.swift: 275", description: "Unable to instantiate comment", phone: "")
                    fatalError("Unable to instantiate comment")
                }
                completionHandler(responce.result, comment)
            }
        })
    }
    
    func saveEvent(name: String, description: String, phone: String) {
        
        if !Net.shared.isConnectedToNetwork() {
            return
        }
        
        let sURL = eventsUrl
        let url:URL = URL(string: sURL)!
        
        let parameters:[String: Any] = ["name": name, "description":description, "phone": phone]
        
        Alamofire.request(url, method: .post, parameters: parameters).response { response in

        }
        
        if(debug == true) {
            print("Event: \(name) Description: \(description)")
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    func getProviderInfo(number: String, completionHandler: @escaping (_ data: Provider) -> Void) {
        //Implementing URLSession
        let urlString = "https://www.megafon.ru/api/mfn/info?msisdn=\(number)"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            //Implement JSON decoding and parsing
            do {
                //Decode retrived data with JSONDecoder and assing type of Article object
                let articlesData = try JSONDecoder().decode(Provider.self, from: data)
                
                //Get back to the main queue
                DispatchQueue.main.async {
                    completionHandler(articlesData)
                    
                }
                
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
        //End implementing URLSession
    }
}

extension String {
    var numbers: String {
        return String(describing: filter { String($0).rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil })
    }
}
