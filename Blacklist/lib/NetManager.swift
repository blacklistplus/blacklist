//
//  NetworkManager.swift
//  Blacklist
//
//  Created by Anton Sokolov on 06.09.2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import UIKit
import Alamofire
import PromiseKit

typealias Token = String

class NetManager {
    let keyUrl = "https://api.ktozvonit.org/getkey.html"
    let commentsUrl = "https://api.ktozvonit.org/rest/comments/"
    let phonesUrl = "https://api.ktozvonit.org/rest/phones/"
    let typesUrl = "https://api.ktozvonit.org/rest/types/"
    var token: String?
    
    static let shared = NetManager()
    private init() { }
    
    func showNetworkActivityIndicator() {
        main {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func hideNetworkActivityIndicator() {
        main {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    
    func getToken() -> Promise<Token> {
        
        return Promise { seal in
            
            if let token = token {
                seal.fulfill(token)
                return
            }
            
            Alamofire.request(keyUrl).responseString { response in
                switch response.result {
                case .success:
                    guard let key = response.value, key.count < 20 else {
                        seal.reject(BlackError.EmptyToken)
                        return
                    }
                    
                    self.token = key
                    seal.fulfill(key)
                    
                case .failure(let error):
                    seal.reject(error)
                }
            }
        }
    }
    
    func getComments(for number: String) -> Promise<([Comment], String?, String?)> {
        
        return Promise { seal in
            firstly { () -> Promise<Token> in
                showNetworkActivityIndicator()
                return getToken()
                }.done { [weak self] token in
                    guard let self = self else { return }
                    let url = self.commentsUrl + "?search[number]=" + number + "&token=\(token)"
                    
                    Alamofire.request(url).responseJSON(completionHandler: {
                        responce in
                        switch responce.result {
                        case .success(let value):
                            guard let data = value as? [String: AnyObject], let rows = data["data"] as? [[String: AnyObject]] else {
                                seal.reject(BlackError.Error(description: "Сервер вернул какую-то ерунду."))
                                return
                            }
                            
                            let comments = rows.map { Comment(with: $0) }.compactMap { $0 }
                            
                            seal.fulfill((comments, data["operator"] as? String, data["region"] as? String))
                            break
                        case .failure(let error):
                            seal.reject(error)
                            break
                        }
                    })
                }.ensure {
                    self.hideNetworkActivityIndicator()
                }.catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Возвращает массив номеров
    ///
    ///
    /// - parameter offset:         смещение, необходимое для выборки определенного подмножества записей.
    /// - parameter limit:          количество записей, которое необходимо получить.
    /// - parameter lastupdate:     время добавления или изменения номера, возвращает только новые номера. В формате unix timestamp.
    /// - parameter completionHandler: замыкание, выполняемое по заврешению запроса
    ///
    /// - returns: Void.
    func getPhones(offset: Int = 0, limit: Int = 2000, updated: Int = 0) -> Promise<([Phone], Int, Int, Int, Int)> {
        
        return Promise { seal in
            firstly { () -> Promise<Token> in
                showNetworkActivityIndicator()
                
                return getToken()
                }.done { [weak self] token in
                    guard let self = self else { return }
                    let url = self.phonesUrl + "?limit=\(limit)&offset=\(offset)&updated=\(updated)&token=\(token)"
                    
                    // Alamofire будем запускать в background чтобы приложение не тормозило во время запросов к серверу
                    let queue = DispatchQueue(label: "DispatchQueueBackground", qos: .background, attributes: .concurrent)
                    
                    Alamofire.request(url).responseData(queue: queue, completionHandler: {
                        responce in
                        
                        switch responce.result {
                        case .success(let data):
                            
                            var phones = [Phone]()
                            
                            do {
                                //Decode retrived data with JSONDecoder and assing type of Article object
                                let result = try JSONDecoder().decode(PhonesJson.self, from: data)
                                phones = result.phones
                                
                                let value = (phones: phones, offset: offset, limit: limit, total: result.total, updated: result.updated)
                                seal.fulfill(value)
                                
                            } catch let jsonError {
                                seal.reject(jsonError)
                                break
                            }
                            
                        case .failure(let error):
                            seal.reject(error)
                            break
                        }
                    })

                }.ensure {
                    self.hideNetworkActivityIndicator()
                }.catch { error in
                    seal.reject(error)
            }
        }
    }
    
    func getTypes() -> Promise<[CommentType]> {
        return Promise { seal in
            firstly { () -> Promise<Token> in
                showNetworkActivityIndicator()
                return getToken()
                }.done { [weak self] token in
                    guard let self = self else { return }
                    let url = self.typesUrl + "?token=\(token)"
                    
                    Alamofire.request(url).responseJSON(completionHandler: {
                        responce in
                        switch responce.result {
                        case .success:
                            
                            guard let value = responce.result.value as? [String: AnyObject],
                                let rows = value["data"] as? [[String: AnyObject]] else {
                                    Net.shared.saveEvent(name: "Net.swift: 154", description: "Malformed data received from fetchAllRooms service", phone: "")
                                    Net.shared.saveEvent(name: "Net.swift: 154", description: "response: \(responce.result.value!)", phone: "")
                                    print("Net.swift 154: Malformed data received from fetchAllRooms service", responce.result.value!)
                                    
                                    return
                            }
                            
                            var types = [CommentType]()
                            
                            for row in rows {
                                if row["id"] != nil && row["name"] != nil {
                                    guard let id = row["id"] as? Int, let name = row["name"] as? String else {
                                        Net.shared.saveEvent(name: "Net.swift: 178", description: "Error while getting types", phone: "")
                                        print("Что то пошло не так: типы комментариев не получены.")
                                        
                                        return
                                    }
                                    
                                    guard let commentType = CommentType(id: id, name: name) else {
                                        Net.shared.saveEvent(name: "Net.swift: 185", description: "Unable to instantiate comment", phone: "")
                                        fatalError("Unable to instantiate comment")
                                    }
                                    
                                    types.append(commentType)
                                }
                            }
                            
                            seal.fulfill(types)
                            
                            
                        case .failure(let error):
                            ErrorHandler.shared.reportError(message: "NetManager: 205")
                            seal.reject(error)
                        }
                    })
                    
                }.ensure {
                    self.hideNetworkActivityIndicator()
                }.catch { error in
                    seal.reject(error)
            }
        }
        
    }
    
    func saveComment(phone: String, name: String, type_id: Int) -> Promise<Comment> {
        return Promise { seal in
            firstly { () -> Promise<Token> in
                showNetworkActivityIndicator()
                return getToken()
                }.done { [weak self] token in
                    guard let self = self else { return }
                    let url = self.commentsUrl
                    let parameters:[String: Any] = ["phone": phone, "name": name, "type_id": type_id, "token": token]
                    
                    Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: {
                        responce in
                        
                        switch responce.result {
                        case .success:
                            guard let value = responce.result.value as? [String: AnyObject], let success = value["success"] as? Bool else {
                                    ErrorHandler.shared.reportError(message: "NetManager: 236 | \(responce.result.value ?? "")")
                                    seal.reject(BlackError.Error(description: "Мне очень жаль, но сейчас программа глючит."))
                                    return
                            }
                            
                            if success {
                                guard let data = value["data"] as? [String: AnyObject] else {
                                        ErrorHandler.shared.reportError(message: "NetManager: 245 | \(responce.result.value ?? "")")
                                        seal.reject(BlackError.Error(description: "Хм, похоже сегодня не получится поработать. Сервер не отвечает."))
                                        return
                                }
                                
                                
                                guard let number = data["phone"] as? String, let name = data["name"] as? String, let avatar = data["avatar"] as? String, let phonetype = data["type"] as? String, let date = data["createdon"] as? String else {
                                    ErrorHandler.shared.reportError(message: "NetManager: 243")
                                    seal.reject(BlackError.Error(description: "Вот дела, ответ есть, но не могу его понять... Попробуйте повторить запрос позже."))
                                    return
                                }
                                
                                guard let comment = Comment(name: name, phone: number, avatar: avatar, type: phonetype, date: date) else {
                                    ErrorHandler.shared.reportError(message: "NetManager: 249")
                                    seal.reject(BlackError.Error(description: "Так-с, что-то пошло не так. Разработчики уже в курсе и исправляют ситуацию."))
                                    return
                                }
                                
                                seal.fulfill(comment)
                                
                            } else {
                                guard let message = value["message"] as? String else {
                                    ErrorHandler.shared.reportError(message: "NetManager: 267")
                                    seal.reject(BlackError.Error(description: "Сегодня не мой день, разработчики уже работают над проблемой."))
                                    return
                                }
                                seal.reject(BlackError.Error(description: message))
                            }
                            
                            
                            
                            
                        case .failure(let error):
                            ErrorHandler.shared.reportError(message: "NetManager: 258")
                            seal.reject(error)
                        }
                    })

                    
                }.ensure {
                    self.hideNetworkActivityIndicator()
                }.catch { error in
                    ErrorHandler.shared.reportError(message: "NetManager: 267")
                    seal.reject(error)
            }
        }
        
    }
    
}
