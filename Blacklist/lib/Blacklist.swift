//
//  Blacklist.swift
//  Blacklist
//
//  Created by Anton Sokolov on 07.08.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import Foundation
import PhoneNumberKit
import CoreData
import CallKit
import UIKit
import YandexMobileMetrica

class Blacklist {
    
    static let shared = Blacklist()
    var isSettingsOn: Bool?
    lazy var phoneNumberKit = PhoneNumberKit()
    
    
    
    var isLicenseAccepted: Bool {
        get {
            guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else {
                return false
            }
            return userDefaults.object(forKey: "LicenseAgreement") as? Date != nil
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                
                Net.shared.saveEvent(name: "Blacklist.swift: 55", description: "Unresolved error \(error), \(error.userInfo)", phone: "")
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var viewContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    lazy var updateContext: NSManagedObjectContext = {
        let _updateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        _updateContext.parent = self.viewContext
        return _updateContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                Net.shared.saveEvent(name: "Blacklist.swift: 73", description: "Unresolved error \(nserror), \(nserror.userInfo)", phone: "")
            }
        }
    }
    
    func saveUpdateContext() {
        let context = updateContext
        if context.hasChanges {
            do {
                try context.save()
                viewContext.performAndWait {
                    do {
                        try viewContext.save()
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    
    func UpdateIdentificationPhoneNumbers() -> (numbers: [String], labels: [String]) {
        var numbers = [String]()
        var labels = [String]()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Number")
        let numberSortDescriptor = NSSortDescriptor(key: "number", ascending: true)
        
        if let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") {
            if (userDefaults.object(forKey: "ShowCompanies") as? Bool) == false {
                // Не показываем компании
                let predicate = NSPredicate(format: "type IN %@", ["Спам", "Банк", "Мошенник", "Телемаркет", "Коллектор", "Опрос", "Колл-центр", "Немой звонок"])
                request.predicate = predicate
            }
        }
        
        
        request.sortDescriptors = [numberSortDescriptor]
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try viewContext.fetch(request)
            print("### count:", result.count)
            for data in result as! [NSManagedObject] {
                numbers.append(String(data.value(forKey: "number") as! Int64))
                if let label = data.value(forKey: "label") as? String, label.count > 0 {
                    labels.append(label)
                } else {
                    labels.append(data.value(forKey: "type") as! String)
                }
                
            }
        } catch {
            Net.shared.saveEvent(name: "Blacklist.swift: 103", description: "UpdateIdentificationPhoneNumbers", phone: "")
            print("Failed", "UpdateIdentificationPhoneNumbers")
        }
        
        return (numbers, labels)

    }
    
    func writeFileForCallDirectory(numbers: [String], labels: [String]) {

    }
    
    
    func CheckUserSettings(completionHandler: @escaping (_ result: Bool) -> Void) {
        CXCallDirectoryManager.sharedInstance.getEnabledStatusForExtension(withIdentifier: "deasoft.blacklist.BlacklistCallExtension", completionHandler: {
            (status, error) in
            
            if(status == CXCallDirectoryManager.EnabledStatus.enabled) {
                print("Обновление разрешено")
                self.isSettingsOn = true
                completionHandler(true)
            } else {
                self.isSettingsOn = false
                completionHandler(false)
            }
        })
    }
    
    
    func updateCallDirectory() {
            let (numbers, labels) = self.UpdateIdentificationPhoneNumbers()
            
            CXCallDirectoryManager.sharedInstance.getEnabledStatusForExtension(withIdentifier: "deasoft.blacklist.BlacklistCallExtension", completionHandler: {
                (status, error) -> Void in
                
                if(status == CXCallDirectoryManager.EnabledStatus.enabled) {
                    guard let fileUrl = FileManager.default
                        .containerURL(forSecurityApplicationGroupIdentifier: "group.deasoft.BlacklistApp")?
                        .appendingPathComponent("AllNumbers") else { return }
                    
                    var string = ""
                    for (number, label) in zip(numbers, labels) {
                        string += "\(number),\(label)\n"
                    }
                    
                    try? string.write(to: fileUrl, atomically: true, encoding: .utf8)
                    
                    CXCallDirectoryManager.sharedInstance.reloadExtension(withIdentifier:"deasoft.blacklist.BlacklistCallExtension", completionHandler: {
                        (error) ->
                        
                        Void in if let error = error {
                            print(error)
                            
                            
                        }
                    })
                }
            })
        }

    
    private init() { }
}




extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count && self.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

extension String {
    var isCorrectPhoneNumber: Bool {
        
        do {
            _ = try Blacklist.shared.phoneNumberKit.parse(self, ignoreType: true)
            return true
        }
        catch {
            return false
        }
    }
    
    var normalizePartial: String {
        if(self.first == "+") {
            return self // нечего нормализовывать
        }
        // Заменяем 810 на +7
        let phoneNumber = PartialFormatter().formatPartial(self)
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let phoneCountryCode = Blacklist.shared.phoneNumberKit.countryCode(for: countryCode) {
            print("countryCode:", countryCode, "phoneCountryCode", phoneCountryCode)
            if(phoneNumber.containsWhitespace) {
                let r = NSMakeRange(0, NSString(string: phoneNumber).range(of: " ").lowerBound)
                print("r:", r, ":", r.lowerBound, r.upperBound)
                let startIndex = phoneNumber.startIndex
                let endIndex = phoneNumber.index(startIndex, offsetBy: r.upperBound)
                
                print("r:", r, ":", phoneNumber[startIndex..<endIndex])
                
                if phoneCountryCode == 7, phoneNumber[startIndex..<endIndex] == "810" {
                    let newNumber = NSString(string: phoneNumber).replacingCharacters(in: r, with: "+")
                    return newNumber.formatAsNumber
                }
                if phoneCountryCode == 7, phoneNumber[startIndex..<endIndex] == "8" {
                    let newNumber = NSString(string: phoneNumber).replacingCharacters(in: r, with: "+7")
                    return newNumber.formatAsNumber
                }
            }
        }
        return self
    }
}

extension String {
    var formatAsPhoneNumber: String {
        do {
            let phoneNumber = try Blacklist.shared.phoneNumberKit.parse(self, ignoreType: true)
            return Blacklist.shared.phoneNumberKit.format(phoneNumber, toType: .international)
        }
        catch {
            return PartialFormatter().formatPartial(self)
        }
    }
}

extension String {
    var formatAsNumber: String {
        let charSet = CharacterSet.init(charactersIn: "+0123456789")
        var s:String = ""
        for unicodeScalar in self.unicodeScalars
        {
            if charSet.contains(unicodeScalar)
            {
                s.append(String(unicodeScalar))
            }
        }
        return s
    }
    
    var formatAsDigits: String {
        let charSet = CharacterSet.init(charactersIn: "0123456789")
        var s:String = ""
        for unicodeScalar in self.unicodeScalars
        {
            if charSet.contains(unicodeScalar)
            {
                s.append(String(unicodeScalar))
            }
        }
        return s
    }
}

extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}

extension String {
    var formatAsPhone: String {
        guard !self.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: self).range(of: self)
        var number = regex.stringByReplacingMatches(in: self, options: .init(rawValue: 0), range: r, withTemplate: "")
        
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }
        
        
        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)
            
        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
        }
        
        return number
    }
}

