//
//  SupportViewController.swift
//  Blacklist
//
//  Created by Anton Sokolov on 05.09.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import UIKit
import WebKit

class SupportViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.navigationDelegate = self

        self.view.insertSubview(UIImageView(image: UIImage(named: "Background")), at: 0)
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        
        //Прозрачный фон
        //Чтобы показывалась картинка Background, а не белый цвет
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        webView.scrollView.backgroundColor = UIColor.clear
        
        if !Net.shared.isConnectedToNetwork() {
            let alertController = UIAlertController(title: NSLocalizedString("No_internet_connection", comment: ""), message:
                NSLocalizedString("No_internet_connection_message", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: {_ in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        
        let myURL = URL(string:"https://docs.google.com/forms/d/e/1FAIpQLSefrH-CVZ6eOAOcrii446x8buT6GRmY-hIEzwOomnaE77BSYQ/viewform")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * Called when an error occurs while the web view is loading content
     */
    private func webView(webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: NSError) {
        
        if error.code == -1001 { // TIMED OUT:
            // CODE to handle TIMEOUT
            ErrorHandler.shared.reportError(message: "LicenseController: 78")
        } else if error.code == -1003 { // SERVER CANNOT BE FOUND
            // CODE to handle SERVER not found
            ErrorHandler.shared.reportError(message: "LicenseController: 81")
        } else if error.code == -1100 { // URL NOT FOUND ON SERVER
            ErrorHandler.shared.reportError(message: "LicenseController: 83")
            // CODE to handle URL not found
        }
        
        self.showAlert(title: NSLocalizedString("Error", comment: "Ошибка"), message: error.localizedDescription)
    }
    
    /**
     * Handles all HTTP responses, from WKNavigationDelegate
     */
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        guard let statusCode
            = (navigationResponse.response as? HTTPURLResponse)?.statusCode else {
                // if there's no http status code to act on, exit and allow navigation
                decisionHandler(.allow)
                return
        }
        
        if statusCode >= 400 {
            // error has occurred
            ErrorHandler.shared.reportError(message: "LicenseController: 106")
            self.showAlert(title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("SERVER_ERROR", comment: "Ошибка сервера"))
        }
        
        decisionHandler(.allow)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }

}
