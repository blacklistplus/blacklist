//
//  TypeTableViewController.swift
//  Blacklist Project 
//
//  Created by Anton Sokolov on 03.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import Alamofire
import os.log

class TypesTableViewController: UITableViewController {
    let netManager = NetManager.shared
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var commentTypes = [CommentType]()
    var selectedType: CommentType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "Background"))
        
        
        netManager.getTypes()
            .done { responce  in
                self.commentTypes = responce
                self.tableView.reloadData()
            }
            .catch {
                error in
                self.showAlert()
            }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return commentTypes.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TypesTableViewCell", for: indexPath)
        
        // Fetches the appropriate meal for the data source layout.
        let type = commentTypes[indexPath.row]
        
        cell.textLabel?.text = type.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedType = commentTypes[indexPath.row]
        performSegue(withIdentifier: "unwindToComment", sender: self)
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        //cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
    }
    
    
    // MARK: Private functions
    
    func showAlert() {
        let alertController = UIAlertController(title: "", message:
            NSLocalizedString("SERVER_ERROR", comment: "Сервер не отвечает"), preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertAction.Style.default,handler: { _ in
            self.performSegue(withIdentifier: "unwindToComment", sender: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
