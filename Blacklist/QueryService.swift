//
//  QueryService.swift
//  Blacklist
//
//  Created by Anton Sokolov on 02/03/2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import Foundation

class QueryService {
    
    let phonesUrl = "https://api.ktozvonit.org/rest/phones"
    let commentsUrl = "https://api.ktozvonit.org/rest/comments/"
    let keyUrl = "https://api.ktozvonit.org/getkey.html"
    let typesUrl = "https://api.ktozvonit.org/rest/types/"
    let eventsUrl = "https://api.ktozvonit.org/rest/events/"
    var token: String?
    
    typealias JSONDictionary = [String: AnyObject]
    typealias QueryResult = ([Phone], _ offset: Int, _ limit: Int, _ total: Int, _ updated: Int) -> ()
    
    var numbers: [Phone] = []
    var errorMessage = ""
    
    var updated = 0
    var offset = 0
    
    // 1
    let defaultSession = URLSession(configuration: .default)
    // 2
    var dataTask: URLSessionDataTask?
    
    func getPhones(offset: Int = 0, limit: Int = 2000, updated: Int = 0, completionHandler: @escaping QueryResult) {
        // 1
        dataTask?.cancel()
        // 2
        if var urlComponents = URLComponents(string: phonesUrl) {
            urlComponents.query = "limit=\(limit)&offset=\(offset)&updated=\(updated)&token=Net.shared.token"
            // 3
            guard let url = urlComponents.url else { return }
            // 4
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                // 5
                if let error = error {
                    self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    let result = self.parsePhones(data)
                    switch result {
                    case .success(let value):
                        let (total, updated) = value
                        print("total: \(total), updated: \(updated)")
                        self.offset = offset
                        DispatchQueue.main.async {
                            completionHandler(self.numbers, offset, limit, total, updated)
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
            // 7
            dataTask?.resume()
        }
    }
    
    fileprivate func parsePhones(_ data: Data) -> BResult<(Int, Int)> {
        numbers.removeAll()
        do {
            let startTime = Date()
            //Decode retrived data with JSONDecoder and assing type of Article object
            let result = try JSONDecoder().decode(PhonesJson.self, from: data)
            numbers = result.phones
            
            let updateTime = Date().timeIntervalSince(startTime)
            print("### Время обработки [parsePhones]: \(updateTime)")
            return BResult.success((result.total, result.updated))
        } catch let jsonError {
            print(jsonError)
            return BResult.failure(jsonError)
        }
        
    }
    
}

public enum BResult<Value> {
    case success(Value)
    case failure(Error)
    
    /// Returns `true` if the result is a success, `false` otherwise.
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    /// Returns `true` if the result is a failure, `false` otherwise.
    public var isFailure: Bool {
        return !isSuccess
    }
    
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}

enum BlackError: Error {
    case JSONSerialization(description: String)
    case PhoneParameterEncodingFailure(description: String)
    case Error(description: String)
    case EmptyToken
    case ServerError(message: String)
}

extension BlackError: LocalizedError {
    var localizedDescription: String {
        switch self {
        case .Error(let description):
            return "\(description)"
        case .PhoneParameterEncodingFailure(let description):
            return "JSON could not be encoded because of error:\n\(description)"
        case .JSONSerialization(let description):
            return "JSON could not be serialized because of error:\n\(description)"
        case .EmptyToken:
            return "Token is nil"
        case .ServerError(let message):
            return "Server response with error message: \(message)"
        }
        
    }
    
    var errorDescription: String? { return localizedDescription }
}
