//
//  UITextFieldX.swift
//  Blacklist
//
//  Created by Anton Sokolov on 31.08.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import UIKit

class UITextFieldX: UITextField {
    
    override var canResignFirstResponder: Bool {
        get {
            return false
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        return false
    }
    
   
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        print("action: ", action)
//        if action == #selector(NSObject.copy(_:)) {
//            return false
//        }
        return super.canPerformAction(action, withSender: sender)
    }

}
