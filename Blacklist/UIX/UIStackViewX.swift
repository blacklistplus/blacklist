//
//  UIStackViewX.swift
//  Blacklist
//
//  Created by Anton Sokolov on 30.08.2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import UIKit

extension UIStackView {
    func shakeTextField (numberOfShakes : Int, direction: CGFloat, maxShakes : Int) {
        let interval : TimeInterval = 0.05
        
        UIView.animate(withDuration: interval, animations: { () -> Void in
            self.transform = CGAffineTransform(translationX: 5 * direction, y: 0)
            
        }, completion: { (aBool :Bool) -> Void in
            
            if (numberOfShakes >= maxShakes) {
                self.becomeFirstResponder()
                return
            }
            self.shakeTextField(numberOfShakes: numberOfShakes + 1, direction: direction * -1, maxShakes: maxShakes)
        })
    }
}
