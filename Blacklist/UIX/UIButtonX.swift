//
//  UIButton+.swift
//  myCRM
//
//  Created by Anton Sokolov on 30.08.2018.
//  Copyright © 2018 Anton Sokolov. All rights reserved.
//

import UIKit

@IBDesignable class UIButtonX: UIButton {
    
    public override func awakeFromNib() {
        //self.layer.cornerRadius = cornerRadius
    }
    
    // MARK: Public interface
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
}
