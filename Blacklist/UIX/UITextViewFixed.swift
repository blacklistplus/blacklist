//
//  UITextViewFixed.swift
//  Blacklist
//
//  Created by Anton Sokolov on 26.09.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import UIKit

@IBDesignable class UITextViewFixed: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}
