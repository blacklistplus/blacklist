//
//  UIReadOnlyTextField.swift
//  Blacklist
//
//  Created by Anton Sokolov on 25.09.2018.
//  Copyright © 2018 MVK, OOO. All rights reserved.
//

import UIKit

class UIReadOnlyTextField: UITextField {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Avoid keyboard to show up
        self.inputView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Avoid keyboard to show up
        self.inputView = UIView()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // Avoid cut and paste option show up
        if (action == #selector(self.cut(_:))) {
            return false
        } else if (action == #selector(self.paste(_:))) {
            return true
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
    
}
