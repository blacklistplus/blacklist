//
//  SearchViewController.swift
//  myCRM
//
//  Created by Anton Sokolov on 29.08.2018.
//  Copyright © 2018 Anton Sokolov. All rights reserved.
//

import UIKit
import PhoneNumberKit
import os.log
import YandexMobileMetrica

@IBDesignable class SearchViewController: UIViewController {
    // MARK: Properties
    var syncProcessor = SyncProcessor.shared
    var alertController: UIAlertController?
    let network = NetworkManager.sharedInstance
    var isSyncDone = true
    
    //let progressIndicatorView = CircularLoaderView(frame: .zero)
    
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var lblMessage: UILabel!
    //@IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var stackNumber: UIStackView!
    
    let phoneNumberKit = PhoneNumberKit()
    
    var number: String? {
        didSet {
            txtNumber.text = number?.formatAsPhoneNumber
        }
    }
    
    var syncTimer: Timer! // Таймер в котором ждем синхронизацию и обновляем статус бар
    let message = NSLocalizedString("Update_the_database", comment: "Обновляем базу")
    var messageDots = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNumber.delegate = self
        lblMessage.text = NSLocalizedString("Enter_the_phone_number", comment: "Введите номер")
        
        
        //Это надо чтобы обрабатывалось событие willEnterForeground
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        syncManager()
        
        textFromClipboard()
        
        //if UIApplication.shared.applicationState != UIApplication.State.background {
            checkNetworkAndSync()
        //}
        
        addLongPressGesture()
    }
    
    //Это надо чтобы обрабатывалось событие willEnterForeground
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    //Обработка события willEnterForeground
    @objc fileprivate func willEnterForeground() {
        print("will enter foreground")
        textFromClipboard()
    }
    
    @objc func runTimedCode() {
        if(messageDots.count > 3) {
            messageDots = ""
        } else {
            messageDots = messageDots + "."
        }
        let spacesCount = 4 - String(syncProcessor.getSyncProgress()).count
        var spaces = ""
        for _ in 0...spacesCount {
            spaces = spaces + " "
        }
        // Показываем статус обновления базы
        self.lblMessage.text = message + spaces + String(syncProcessor.getSyncProgress()) + "%"
        
        if !syncProcessor.getSyncStatus() {
            DispatchQueue.main.async { [unowned self] in
                self.syncTimer.invalidate()
                
                // После обновления базы показываем сообщение "Актуальная база"
                
                self.lblMessage.text = NSLocalizedString("Current_base", comment: "Актуальная база")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.lblMessage.fadeOut(completion: {
                        (finished: Bool) -> Void in
                        self.lblMessage.text = NSLocalizedString("Enter_the_phone_number", comment: "Введите номер")
                        self.lblMessage.fadeIn()
                    })
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        txtNumber.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Show the Navigation Bar
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // MARK: - Buttons
    
    @IBOutlet var digitButtons: [UIButton]!
    
    
    @IBAction func touchSearch(_ sender: UIButton) {
        if !Net.shared.isConnectedToNetwork() {
            showAlert(title: NSLocalizedString("No_internet_connection", comment: ""), message: NSLocalizedString("No_internet_connection_message", comment: ""))
            return
        }
        
        if(number != nil && number!.formatAsNumber.isCorrectPhoneNumber) {
            performSegue(withIdentifier: "showComments", sender: sender)
        } else {
            stackNumber.shakeTextField(numberOfShakes: 4, direction: -1, maxShakes: 8)
        }
    }
    
    @IBAction func touchDigit(_ sender: UIButton) {
        
        let btnCollection = digitButtons.sorted(by: { $0.tag < $1.tag})
        
        guard let buttonNumber = btnCollection.index(of: sender) else {
            ErrorHandler.shared.reportError(message: "SearchViewController: 140")
            return
        }
        switch buttonNumber {
        case nil:
            print("Button has no index")
        case 10:
            if let number = self.number, !number.isEmpty {
                //number = number! + "+"
            } else {
                number = "+"
            }
            
        case 11:
            if let number = self.number, number.count > 0 {
                self.number = String((number.dropLast()))
            } else {
                self.number = nil
            }
            
        default:
            guard var number = self.number else {
                self.number = String(buttonNumber)
                return
            }
            
            number += String(buttonNumber)

            self.number = number.normalizePartial

        }
        
    }
    
    // MARK: - Navigation
    
    @IBAction func unwindToSearchViewController(segue: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "toSettings":
            os_log("Open Settings window.", log: OSLog.default, type: .debug)
            
        case "toHelp":
            os_log("Prepare Open Help window.", log: OSLog.default, type: .debug)
            
        case "showComments":
            os_log("Segue: Search and show comments", log: OSLog.default, type: .debug)
            
            guard let viewController = segue.destination as? CommentTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            viewController.number = self.number!.formatAsNumber
            viewController.syncProcessor = syncProcessor
            
            
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            
        }
        
        
    }
    
    // MARK: - Private Functions
    
    // MARK: - Sync Process
    
    func syncManager() {
        network.reachability.whenReachable = { _ in
            // Online
            if let alertController = self.alertController {
                DispatchQueue.main.async {
                    alertController.dismiss(animated: true, completion: nil)
                }
            }
        }
        network.reachability.whenUnreachable = { reachability in
            // Offline
            // Показываем уведомление
            self.alertController = UIAlertController(title: NSLocalizedString("No_internet_connection", comment: ""), message:
                NSLocalizedString("No_internet_connection_message", comment: ""), preferredStyle: UIAlertController.Style.alert)
            //self.alertController!.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
            
            self.present(self.alertController!, animated: true, completion: nil)
        }
    }
    
    /**
    * Запускаем синхронизацию базы
    */
    func startSync() {
        
        print("Запускаем Sync из ViewController")
        syncTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        syncProcessor.Sync() { total in
            print("Вернулись в View", total)
        }
    }
    
    
    func checkNetworkAndSync() {
        NetworkManager.isReachable { _ in
            NetworkManager.isReachableViaWiFi {_ in
                self.syncProcessor.isReachableViaWiFi = true
            }
            self.startSync()
        }
        
        Blacklist.shared.CheckUserSettings(completionHandler: { result in
            // Возвращаемся в main thread чтобы получить доступ к UI
            DispatchQueue.main.async { [unowned self] in
                
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[1]
                    if(result == true) {
                        tabItem.badgeValue = nil
                    } else {
                        // Если у пользователя в настройках не разрешен доступ к CallKit
                        // рисуем единичку на табе Настройки
                        tabItem.badgeValue = "1"
                    }
                }
            }
        })
    }
    
    func textFromClipboard() {
        if let myString = UIPasteboard.general.string, myString.formatAsNumber.isCorrectPhoneNumber {
            if number == nil {
                number = myString
            }
        }
    }
    
    @objc func longPress(gesture: UILongPressGestureRecognizer) {        if gesture.state == UIGestureRecognizer.State.began {
        self.number = nil
        }
    }
    
    func addLongPressGesture(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
        longPress.minimumPressDuration = 0.5
        self.digitButtons[11].addGestureRecognizer(longPress)
    }
    
}

// Обработка события past в поле телефонного номера
extension SearchViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        number = string
        return false //prevents editing
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

