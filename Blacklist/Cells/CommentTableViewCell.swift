//
//  CommentTableViewCell.swift
//  Blacklist Project
//
//  Created by Anton Sokolov on 30.08.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var avatarImage: UIImageView!
    //@IBOutlet weak var nameLabel: UITextView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
