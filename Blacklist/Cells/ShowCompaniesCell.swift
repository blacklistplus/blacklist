//
//  ShowCompaniesCell.swift
//  Blacklist
//
//  Created by Anton Sokolov on 12/04/2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import UIKit

class ShowCompaniesCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var showCompaniesSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") {
            if (userDefaults.object(forKey: "ShowCompanies") as? Bool) == false {
                showCompaniesSwitch.isOn = false
            } else {
                showCompaniesSwitch.isOn = true
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func switchChanged(_ sender: UISwitch) {
        guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else {
            Net.shared.saveEvent(name: "ShowCompanies: 27", description: "UserDefaults", phone: "")
            return
        }
        
        if sender.isOn {
            // Включаем определение компаний
            userDefaults.set(true, forKey: "ShowCompanies")
        } else {
            // Отключаем определение компаний
            userDefaults.set(false, forKey: "ShowCompanies")
        }
        
        userDefaults.synchronize()
    }
}
