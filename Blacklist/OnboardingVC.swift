//
//  OnboardingVC.swift
//  TestProject
//
//  Created by Anton Sokolov on 25.08.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import SwiftyOnboard

class OnboardingVC: UIViewController {
    
    var swiftyOnboard: SwiftyOnboard!
    let colors:[UIColor] = [#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)]
    var titleArray: [String] = [NSLocalizedString("OnboardTitle1", comment: ""), NSLocalizedString("OnboardTitle2", comment: ""), NSLocalizedString("OnboardTitle3", comment: "")]
    var subTitleArray: [String] = [NSLocalizedString("OnboardSubTitle1", comment: ""), NSLocalizedString("OnboardSubTitle2", comment: ""), NSLocalizedString("OnboardSubTitle3", comment: "")]
    
    var gradiant: CAGradientLayer = {
        //Gradiant for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [purple, blue]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    let NumberOfPagesInTheOnboarding = 3
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super .viewDidLoad()
        
        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .dark)
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
        //view.continueButton.setTitle(NSLocalizedString("Continue", comment: ""), for: .normal)
        
        
        //welcomeImageView.kf.setImage(with: URL(string: "https://robohash.org/blacklist.png?bgset=bg1"))
    }
    
    func gradient() {
        //Add the gradiant to the view:
        self.gradiant.frame = view.bounds
        view.layer.addSublayer(gradiant)
    }
    
    @objc func handleSkip() {
        performSegue(withIdentifier: "toLicenseAgreementSegue", sender: self)
    }
    
    @objc func handleContinue(sender: UIButton) {
        let index = sender.tag
        if index == 2 {
            performSegue(withIdentifier: "toLicenseAgreementSegue", sender: self)
        } else {
            swiftyOnboard.goToPage(index: index + 1, animated: true)
        }
    }
    
    
}

extension OnboardingVC: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //Number of pages in the onboarding:
        return NumberOfPagesInTheOnboarding
    }
    
//    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
//        //Return the background color for the page at index:
//        return colors[index]
//    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        
        //Set the image on the page:
        view.imageView.image = UIImage(named: "onboard\(index)")
        
        //Set the font and color for the labels:
        view.title.font = UIFont.preferredFont(forTextStyle: .title2)   //systemFont(ofSize: 22, weight: UIFontWeightSemibold)
        //view.title.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.subTitle.font = UIFont.preferredFont(forTextStyle: .body)
        
        //Set the text in the page:
        view.title.text = titleArray[index]
        view.subTitle.text = subTitleArray[index]
        
        //Return the page for the given index:
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        
        //Setup targets for the buttons on the overlay view:
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        
        
        //Setup for the overlay buttons:
        overlay.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        //overlay.continueButton.setTitleColor(UIColor.red, for: .normal)
        overlay.skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        //overlay.skipButton.setTitleColor(UIColor.red, for: .normal)
        
        overlay.continueButton.setTitle(NSLocalizedString("Continue", comment: ""), for: .normal)
        overlay.skipButton.setTitle("", for: .normal)
        overlay.skipButton.isHidden = true
        //overlay.skipButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        
        //Return the overlay view:
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.continueButton.tag = Int(position)
        
        if currentPage < Double(NumberOfPagesInTheOnboarding - 1) {
            overlay.continueButton.setTitle(NSLocalizedString("Continue", comment: ""), for: .normal)
            overlay.skipButton.setTitle("", for: .normal)
            overlay.skipButton.isHidden = true
            //overlay.skipButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
            overlay.skipButton.isHidden = false
//            overlay.continueButton.layer.borderColor = UIColor.black.cgColor
//            overlay.continueButton.layer.borderWidth = 1
//            overlay.continueButton.layer.cornerRadius = overlay.continueButton.bounds.height / 2
        } else {
            overlay.continueButton.setTitle(NSLocalizedString("Get_Started", comment: ""), for: .normal)
            overlay.skipButton.isHidden = true
        }
    }
}
