//
//  CommentTableViewController.swift
//  Blacklist Project
//
//  Created by Anton Sokolov on 30.08.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import os.log
import PhoneNumberKit

class CommentTableViewController: UITableViewController {
    
    //MARK: Properties
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var syncProcessor: SyncProcessor!
    let netManager = NetManager.shared
    var comments = [Comment]()
    var number: String!
    var phoneInfo: String? {
        didSet {
            if let info = phoneInfo {
                phoneInfoLabel.text = info
            }
        }
    }
    //let backgroundImage = UIImage(named: "Background")
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private var roundButton = UIButton()
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareView()
        getComments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        createFloatingButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                //self.roundButton = nil
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareView() {
        tableView.backgroundView = UIImageView(image: UIImage(named: "Background"))
        
        phoneNumberLabel.text = number.formatAsPhoneNumber
        
        // Определяем провайдера
        Net.shared.getProviderInfo(number: number) {
            provider in
            self.phoneInfo = provider.region  + " | " + provider.name
        }
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CommentTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CommentTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let comment = comments[indexPath.row]
        
        cell.nameLabel.text = comment.name
        cell.avatarImage.kf.setImage(with: comment.avatarURL)
        cell.avatarImage.layer.cornerRadius = cell.avatarImage.frame.size.width/2
        cell.avatarImage.clipsToBounds = true
        cell.typeLabel.text = comment.type
        cell.dateLabel.text = comment.date
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        //cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }

    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "addComment":
            
            os_log("Adding a new comment.", log: OSLog.default, type: .debug)
            guard let newCommentViewController = segue.destination as? CommentViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            //newCommentViewController.phoneNumber = phoneNumber
            newCommentViewController.number = number
            
        case "unwindToSearchViewController":
            os_log("unwindToViewController", log: OSLog.default, type: .debug)
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            
        }
        
    }
    
    
    
    //MARK: Actions
    
    @IBAction func unwindToCommentList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? CommentViewController, let comment = sourceViewController.comment {
            
            netManager.saveComment(phone: comment.phone, name: comment.name, type_id: comment.type_id!)
                .done { comment in
                    self.getComments()
                }
                .catch { error in
                    self.showAlert(title: NSLocalizedString("Error", comment: "Ошибка"), message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Private functions
    
    func getComments() {
        netManager.getComments(for: self.number)
            .done { comments, provider, region in
                if self.phoneInfo == nil, let region = region, let provider = provider {
                    self.phoneInfo = region + " | " + provider
                }
                if(comments.count == 0) {
                    let alertController = UIAlertController(title: "", message:
                        NSLocalizedString("NO_COMMENTS", comment: "Отзывов нет. Вы можете оставить первый."), preferredStyle: UIAlertController.Style.alert)
                    
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Отмена"), style: UIAlertAction.Style.default,handler: { _ in
                        NSLog("The \"Отзыв\" alert occured.")
                        self.performSegue(withIdentifier: "unwindToSearchViewController", sender: nil)
                    }))
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Comment", comment: "Отзыв"), style: UIAlertAction.Style.default,handler: { _ in
                        NSLog("The \"Отзыв\" alert occured.")
                        self.performSegue(withIdentifier: "addComment", sender: nil)
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    self.comments = comments
                    self.syncProcessor.Sync() { total in
                        print("База синхронизирована из TableView", total)
                    }
                    self.tableView.reloadData()
                }
                
            }
            .catch {
                error in
                self.showAlert(title: "", message: NSLocalizedString("SERVER_ERROR", comment: "Ошибка сервера"))
        }
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = .white
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"NewComment"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(self.addNewComment(_:)), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 14),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 14),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 65),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 65)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 33.5
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.05;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
        }
    }
    
    @objc func addNewComment(_ sender: UIButton) {
        performSegue(withIdentifier: "addComment", sender: nil)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

struct Article: Codable {
    let provider: String
    let operator_id: Int
    let region: String
    let region_id: Int
    
    enum CodingKeys: String, CodingKey {
        case provider = "operator"
        case operator_id
        case region
        case region_id
    }
}
