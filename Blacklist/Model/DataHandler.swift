//
//  DataHandler.swift
//  Blacklist
//
//  Created by Anton Sokolov on 18/02/2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import CoreData

class DataHandler {
    
    // MARK: Properties
    private let modelName: String
    
    lazy var mainContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    lazy var storeContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: self.modelName)
        self.seedCoreDataContainerIfFirstLaunch()
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        
        return container
    }()
    
    // MARK: Initializers
    init(modelName: String) {
        self.modelName = modelName
    }
}

// MARK: Internal
extension DataHandler {
    
    func saveContext () {
        guard mainContext.hasChanges else { return }
        
        do {
            try mainContext.save()
        } catch let nserror as NSError {
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
}

// MARK: Private
private extension DataHandler {
    
    func seedCoreDataContainerIfFirstLaunch() {
        
        // 1: Check if first run
        if UserDefaults.standard.value(forKey: "Updated23") as? Int != nil {
            return
        }
            
        // Default directory where the CoreData will store its files
        let directory = NSPersistentContainer.defaultDirectoryURL()
        let url = directory.appendingPathComponent(modelName + ".sqlite")
        
        // 2: Copying the SQLite file
        let seededDatabaseURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite")!
        _ = try? FileManager.default.removeItem(at: url)
        do {
            try FileManager.default.copyItem(at: seededDatabaseURL, to: url)
        } catch let nserror as NSError {
            fatalError("Error: \(nserror.localizedDescription)")
        }
        
        // 3: Copying the SHM file
        let seededSHMURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite-shm")!
        let shmURL = directory.appendingPathComponent(modelName + ".sqlite-shm")
        _ = try? FileManager.default.removeItem(at: shmURL)
        do {
            try FileManager.default.copyItem(at: seededSHMURL, to: shmURL)
        } catch let nserror as NSError {
            fatalError("Error: \(nserror.localizedDescription)")
        }
        
        // 4: Copying the WAL file
        let seededWALURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite-wal")!
        let walURL = directory.appendingPathComponent(modelName + ".sqlite-wal")
        _ = try? FileManager.default.removeItem(at: walURL)
        do {
            try FileManager.default.copyItem(at: seededWALURL, to: walURL)
        } catch let nserror as NSError {
            fatalError("Error: \(nserror.localizedDescription)")
        }
        
        UserDefaults.standard.set(1599724414, forKey: "Updated")
        UserDefaults.standard.set(1599724414, forKey: "Updated23")
        print("Seeded Core Data")
        
    }
}

extension DataHandler {
    func seedIdentificationPhoneNumbers() {
        
        if UserDefaults.standard.value(forKey: "Updated23") as? Int != nil {
            return
        }
        
        var numbers = [String]()
        var labels = [String]()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Number")
        let numberSortDescriptor = NSSortDescriptor(key: "number", ascending: true)
        request.sortDescriptors = [numberSortDescriptor]
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try mainContext.fetch(request)
            for data in result as! [NSManagedObject] {
                numbers.append(String(data.value(forKey: "number") as! Int64))
                if let label = data.value(forKey: "label") as? String, label.count > 0 {
                    labels.append(label)
                } else {
                    labels.append(data.value(forKey: "type") as! String)
                }
                
            }
        } catch {
            print("Failed", "UpdateIdentificationPhoneNumbers")
        }
        
        guard let fileUrl = FileManager.default
            .containerURL(forSecurityApplicationGroupIdentifier: "group.deasoft.BlacklistApp")?
            .appendingPathComponent("AllNumbers") else { return }
        
        var string = ""
        for (number, label) in zip(numbers, labels) {
            string += "\(number),\(label)\n"
        }
        
        try? string.write(to: fileUrl, atomically: true, encoding: .utf8)
    }
}
