//
//  Provider.swift
//  Blacklist
//
//  Created by Anton Sokolov on 22/02/2019.
//  Copyright © 2019 MVK, OOO. All rights reserved.
//

import Foundation

struct Provider: Codable {
    let name: String
    let operator_id: Int
    let region: String
    let region_id: Int
    
    enum CodingKeys: String, CodingKey {
        case name = "operator"
        case operator_id
        case region
        case region_id
    }
}
