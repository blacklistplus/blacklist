 //
//  Comment.swift
//  TestProject
//
//  Created by Anton Sokolov on 29.08.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import Alamofire

class Comment {
    
    //MARK: Properties
    
    var name: String
    var phone: String
    var avatar: String?
    var avatarURL: URL?
    var type: String?
    var date: String?
    var type_id: Int?
    
    let robohashURL = "https://robohash.org/"
    let robohashParamURL = "?bgset=bg1&size=48x48"
    
    //MARK: Initialization
    
    init?(name: String, phone: String, avatar: String, type: String, date: String) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        guard !phone.isEmpty else {
            return nil
        }
        
        
        
        // Initialize stored properties.
        self.name = name
        self.phone = phone
        self.avatar = avatar
        self.avatarURL = URL(string: robohashURL + avatar + ".png" + robohashParamURL)!
        self.type = type
        self.date = date
    }
    
    init?(name: String, phone: String, type_id: Int) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        guard !phone.isEmpty else {
            return nil
        }
        
        
        
        // Initialize stored properties.
        self.name = name
        self.phone = phone
        self.type_id = type_id
    }
    
    init?(with dictionary: [String: Any]?) {
        guard let dictionary = dictionary else { return nil }
        guard let name = dictionary["name"] as? String, let phone = dictionary["phone"] as? String, let avatar = dictionary["avatar"] as? String, let type = dictionary["type"] as? String, let date = dictionary["createdon"] as? String else {
            ErrorHandler.shared.reportError(message: "Пустые поля у комментария")
            return nil
        }
        self.name = name
        self.phone = phone
        self.avatar = avatar
        self.avatarURL = URL(string: robohashURL + avatar + ".png" + robohashParamURL)!
        self.type = type
        self.date = date
    }
    
    func save(completionHandler: @escaping (_ comment: Comment) -> Void) {
        let commentsUrl = "https://ktozvonit.org/rest/comments/"
        let sURL = commentsUrl
        let url:URL = URL(string: sURL)!
        
        let parameters:[String: Any] = ["phone": phone, "name": name, "type_id": type_id!]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: {
            responce in
            switch responce.result {
            case .success:
                guard let value = responce.result.value as? [String: AnyObject],
                    let data = value["data"] as? [String: AnyObject] else {
                        print("Malformed data received from fetchAllRooms service")
                        
                        return
                }
                
                
                
                if data["name"] != nil && data["phone"] != nil {
                    guard let number = data["phone"] as? String, let name = data["name"] as? String, let avatar = data["avatar"] as? String, let phonetype = data["type"] as? String, let date = data["createdon"] as? String else {
                        print("что то пошло не так")
                        return
                    }
                    
                    guard let comment = Comment(name: name, phone: number, avatar: avatar, type: phonetype, date: date) else {
                        fatalError("Unable to instantiate comment")
                    }
                    completionHandler(comment)
                }
                
                
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
}
