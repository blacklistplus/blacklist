//
//  phone.swift
//  Blacklist
//
//  Created by Anton Sokolov on 05.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import PhoneNumberKit
import Alamofire

class Phone: Decodable {
    
    //MARK: Properties
    
    lazy var phoneNumberKit = PhoneNumberKit()
    
    var number: String
    var label: String
    var type: String
    var type_id: Int16
    var editedon: Int
    
    var numberFormatted: String {
        get {
            return formatPhoneNumber(phone: number)
        }
        set{}
    }
    var numberString: String {
        get {
            return getString(phone: number)
        }
        set{}
    }
    

    enum CodingKeys: String, CodingKey {
        case number
        case label
        case type
        case type_id
        case editedon
    }
    
    
    //MARK: Initialization
    
    init?(number: String, label: String, type: String, type_id: Int16, editedon: Int) {
        
        // Initialization should fail if there is no number or label.
        
//        guard !label.isEmpty else {
//            return nil
//        }
        
        // The number must not be empty
        guard !number.isEmpty else {
            return nil
        }
        
        
        
        // Initialize stored properties.
        self.number = number
        self.label = label
        self.type = type
        self.type_id = type_id
        self.editedon = editedon
        //self.numberFormatted = formatPhoneNumber(phone: number)
        
    }
    
    init?(number: String) {
        
        // Initialization should fail if there is no number or label.
        
        //        guard !label.isEmpty else {
        //            return nil
        //        }
        
        // The number must not be empty
        guard !number.isEmpty else {
            return nil
        }
        
        
        
        // Initialize stored properties.
        self.number = number
        self.label = ""
        self.type = ""
        self.type_id = 0
        self.editedon = 0
        //self.numberFormatted = formatPhoneNumber(phone: number)
        
    }
    
    init?(with dictionary: [String: Any]?) {
        guard let dictionary = dictionary else { return nil }
        guard let number = dictionary["number"] as? String, let label = dictionary["label"] as? String, let type = dictionary["type"] as? String, let type_id = dictionary["type_id"] as? Int16, let editedon = dictionary["editedon"] as? Int else {
            ErrorHandler.shared.reportError(message: "Пустые поля у телефона")
            return nil
        }
        self.number = number
        self.label = label
        self.type = type
        self.type_id = type_id
        self.editedon = editedon
    }
    
    // MARK: Private functions
    func formatPhoneNumber(phone: String) -> String {
        
        do {
            let phoneNumber = try phoneNumberKit.parse(phone)
            return (phoneNumberKit.format(phoneNumber, toType: .international))
        }
        catch {
            return(number)
        }
    }
    
    func isCorrectInternationalNumber() -> Bool {
        
        do {
            let phoneNumber = try phoneNumberKit.parse(number)
            let _ = phoneNumberKit.format(phoneNumber, toType: .national)
            return true
        }
        catch {
            return false
        }
    }
    
    func getString(phone: String) -> String {
        
        do {
            let phoneNumber = try phoneNumberKit.parse(phone)
            return (phoneNumberKit.format(phoneNumber, toType: .e164))
        }
        catch {
            return(number)
        }
    }
    
//    func getPhoneComments(completionHandler: @escaping (_ comments: [Comment]) -> Void) {
//        
//        let commentsUrl = "https://ktozvonit.org/rest/comments/"
//        let sURL = commentsUrl + "?search[number]=" + self.numberString
//        let url:URL = URL(string: sURL)!
//        
//        Alamofire.request(url).responseJSON(completionHandler: {
//            responce in
//            switch responce.result {
//            case .success:
//                
//                guard let value = responce.result.value as? [String: AnyObject],
//                    let rows = value["data"] as? [[String: AnyObject]] else {
//                        print("Malformed data received from fetchAllRooms service")
//                        
//                        return
//                }
//                
//                var comments = [Comment]()
//                
//                for row in rows {
//                    if row["name"] != nil && row["phone"] != nil {
//                        guard let number = row["phone"] as? String, let name = row["name"] as? String, let avatar = row["avatar"] as? String, let phonetype = row["type"] as? String, let date = row["createdon"] as? String else {
//                            print("что то пошло не так")
//                            return
//                        }
//                        
//                        guard let comment = Comment(name: name, phone: number, avatar: avatar, type: phonetype, date: date) else {
//                            fatalError("Unable to instantiate comment")
//                        }
//                        
//                        comments.append(comment)
//                    }
//                }
//                
//                completionHandler(comments)
//                //Uncomment for debugginng
//                //Thread.sleep(forTimeInterval: 3)
//                
//            case .failure(let error):
//                print(error)
//            }
//        })
//        
//    }
    
}

struct PhonesJson : Decodable {
    
    enum  CodingKeys: String, CodingKey {
        case total, updated, phones = "data"
    }
    
    let total : Int
    let updated: Int
    let phones : [Phone]
}
