//
//  CommentType.swift
//  TestProject
//
//  Created by Anton Sokolov on 03.09.17.
//  Copyright © 2017 Anton Sokolov. All rights reserved.
//

import UIKit
import Alamofire

class CommentType {
    
    //MARK: Properties
    
    var id: Int
    var name: String

    
    //MARK: Initialization
    
    init?(id: Int, name: String) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        guard id >= 0 else {
            return nil
        }
        
        
        
        // Initialize stored properties.
        self.id = id
        self.name = name
        
    }
    
    class func getTypesList(completionHandler: @escaping (_ types: [CommentType], _ success: Bool) -> Void) {
        
        let typesUrl = "https://ktozvonit.org/rest/types/"
        let sURL = typesUrl
        let url:URL = URL(string: sURL)!
        
        Alamofire.request(url).responseJSON(completionHandler: {
            responce in
            switch responce.result {
            case .success:
                
                guard let value = responce.result.value as? [String: AnyObject],
                    let rows = value["data"] as? [[String: AnyObject]] else {
                        print("CommentType.swift 55: Malformed data received from fetchAllRooms service")
                        
                        return
                }
                
                var types = [CommentType]()
                
                for row in rows {
                    if row["id"] != nil && row["name"] != nil {
                        guard let id = row["id"] as? Int, let name = row["name"] as? String else {
                            print("Что то пошло не так: типы комментариев не получены.")
                            
                            return
                        }
                        
                        guard let commentType = CommentType(id: id, name: name) else {
                            fatalError("Unable to instantiate comment")
                        }
                        
                        types.append(commentType)
                    }
                }
                
                completionHandler(types, true)
                
                
            case .failure(let error):
                print(error)
                completionHandler([], false)
            }
        })
        
    }
    
}
