//
//  ViewController.swift
//  LicenseVC
//
//  Created by Anton Sokolov on 24.09.2018.
//  Copyright © 2018 Anton Sokolov. All rights reserved.
//

import UIKit
import WebKit
class LicenseAgreementVC: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.navigationDelegate = self
        
        self.view.insertSubview(UIImageView(image: UIImage(named: "Background")), at: 0)
        
        //Прозрачный фон
        //Чтобы показывалась картинка Background, а не белый цвет
        webView.isOpaque = false
        webView.backgroundColor = #colorLiteral(red: 0.05490196078, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        webView.scrollView.backgroundColor = #colorLiteral(red: 0.05490196078, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        
        //let myURL = URL(string:"https://ktozvonit.org/license-raw.html")
        guard let path = Bundle.main.url(forResource: "www/index", withExtension: "html") else {
            fatalError()
        }
        let url = path
        
        let myRequest = URLRequest(url: url)
        webView.load(myRequest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        if !Net.shared.isConnectedToNetwork() {
            let alertController = UIAlertController(title: NSLocalizedString("No_internet_connection", comment: ""), message:
                NSLocalizedString("No_internet_connection_message", comment: ""), preferredStyle: UIAlertController.Style.alert)
            //alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
    }
    
    /**
     * Handles all HTTP responses, from WKNavigationDelegate
     */
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        guard let statusCode
            = (navigationResponse.response as? HTTPURLResponse)?.statusCode else {
                // if there's no http status code to act on, exit and allow navigation
                decisionHandler(.allow)
                return
        }
        
        if statusCode >= 400 {
            // error has occurred
            print("Error has occurred")
        }
        
        decisionHandler(.allow)
    }
    
    // MARK: - Buttons
    
    @IBAction func touchAgree(_ sender: UIBarButtonItem) {
        //UserDefaults.standard.set(false, forKey: "FirstRun")
        //UserDefaults.standard.set(Date(), forKey: "LicenseAgreement")
        
        guard let userDefaults = UserDefaults(suiteName: "group.deasoft.BlacklistApp") else {
            Net.shared.saveEvent(name: "Net.swift: 42", description: "UserDefaults", phone: "")
            return
        }
        
        userDefaults.set(Date(), forKey: "LicenseAgreement")
        userDefaults.synchronize()
            
        performSegue(withIdentifier: "toMainSegue", sender: self)
    }
    
    @IBAction func touchCancel(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: NSLocalizedString("License_Agreement", comment: ""), message:
            NSLocalizedString("License_Agreement_Cancel", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}

