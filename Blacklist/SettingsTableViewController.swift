//
//  SettingsTableViewController.swift
//  Blacklist
//
//  Created by Anton Sokolov on 12.09.17.
//  Copyright © 2017 MVK, OOO. All rights reserved.
//

import UIKit
import os.log

class SettingsTableViewController: UITableViewController {
    
    let AppleID = "1281082793"
    let appStoreURL = "itms-apps://itunes.apple.com/ru/app/"
    var cells = [
        (id:"review", name:NSLocalizedString("Leave_a_Review", comment: ""), accessoryType:  UITableViewCell.AccessoryType.none),
        (id:"license", name:NSLocalizedString("License_agreement", comment: ""), accessoryType: UITableViewCell.AccessoryType.disclosureIndicator)
    ]

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "Background"))
        
        //Это надо чтобы обрабатывалось событие willEnterForeground
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    //Это надо чтобы обрабатывалось событие willEnterForeground
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    //Обработка события willEnterForeground
    @objc fileprivate func willEnterForeground() {
        Blacklist.shared.CheckUserSettings(completionHandler: { result in
            // Возвращаемся в main thread чтобы получить доступ к UI
            DispatchQueue.main.async { [unowned self] in
                self.prepareView()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        prepareView()
    }
    
    func prepareView() {
        cells = [
            (id:"support", name:NSLocalizedString("TECHNICAL_SUPPORT", comment: "Report problem"), accessoryType: UITableViewCell.AccessoryType.disclosureIndicator),
            //(id:"review", name:NSLocalizedString("Leave_a_Review", comment: ""), accessoryType:  UITableViewCell.AccessoryType.none),
            (id:"license", name:NSLocalizedString("License_agreement", comment: ""), accessoryType: UITableViewCell.AccessoryType.disclosureIndicator)
        ]
        
        if(Blacklist.shared.isSettingsOn == false) {
            cells.append((id: "settings", name: NSLocalizedString("Settings", comment: ""), accessoryType: UITableViewCell.AccessoryType.detailDisclosureButton))
        }  else {
            cells.append((id: "showCompanies", name: NSLocalizedString("ShowCompainies", comment: "Отображать компании"), accessoryType: UITableViewCell.AccessoryType.none))
        }
        self.tableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cells[indexPath.row].id {
            case "showCompanies":
                return 100.00
            default:
                return 50.00
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            switch cells[indexPath.row].id {
            case "review":
                print("Leave a Review")
                if let url = URL(string: appStoreURL + "id" + AppleID + "?action=write-review&mt=8"),
                    UIApplication.shared.canOpenURL(url)
                {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            case "license":
                performSegue(withIdentifier: "showLicense", sender: nil)
            case "settings":
                performSegue(withIdentifier: "showHelp", sender: nil)
            case "support":
                performSegue(withIdentifier: "showSupport", sender: nil)
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                print("Строка 0")
            case 1:
                print("Строка 1")
            default:
                break
            }
        case 2: break
        // Do something
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        //cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cells[indexPath.row].id {
        case "showCompanies":
            let cellIdentifer = "cellShowCompanies"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as! ShowCompaniesCell
            cell.titleLabel.text = cells[indexPath.row].name
            cell.subtitleLabel.text = NSLocalizedString("ShowCompainiesSubtitle", comment: "")
            cell.accessoryType = cells[indexPath.row].accessoryType
            return cell
        default:
            let cellIdentifer = "cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath)
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = #colorLiteral(red: 0.04705882353, green: 0.1019607843, blue: 0.1803921569, alpha: 1)
            cell.selectedBackgroundView = backgroundView
            
            cell.textLabel?.text = cells[indexPath.row].name
            cell.accessoryType = cells[indexPath.row].accessoryType
            
            return cell
        }
        

        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("SETTINGS", comment: "")
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
